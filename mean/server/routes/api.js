const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const ObjectID = require('mongodb').ObjectID;

// Connect
/*
const connection = (closure) => {
    return MongoClient.connect('mongodb://localhost:27017/mean', (err, db) => {
        if (err) return console.log(err);
        else console.log("Connected successfully to server");

        closure(db);
    });
};
*/

const findUsers = function(db, callback) {
    // Get the documents collection
    const collection = db.collection('documents');
    // Find some documents
    collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.log(docs)
        callback(docs);
    });
}


// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};

// Get users
router.get('/users', (req, res) => {
    /*    connection((db) => {
            db.collection('users')
                .find()
                .toArray()
                .then((users) => {
                    response.data = users;
                    res.json(response);
                })
                .catch((err) => {
                    sendError(err, res);
                });
        });*/

    // Use connect method to connect to the server
    MongoClient.connect('mongodb://localhost:27017', function(err, client) {
        assert.equal(null, err);
        console.log("Connected correctly to server");

        const db = client.db('mean');

        findUsers(db, function(users) {
            response.data = users;
            res.json(response);
            client.close();
        });

    });

});

module.exports = router;